﻿using HarmonyLib;
using Pawnmorph.HPatches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_PawnMorpher
{
    /// <summary>
    /// Patches pawnmorphers own retrieval of offset values for capacities to consider quirk influences
    /// Update 29 Aug 2023: PawnMorpher changed their patch into a transpiler, so now it redirects to the base game capacity calculations without any further patching requirements.
    /// </summary>
    //[HarmonyPatch(typeof(PawnCapacityUtilsPatch), "GetTotalCapacityOffset")]
    //public static class Patch_PawnCapacityUtilsPatch
    //{
    //    [HarmonyTranspiler]
    //    public static IEnumerable<CodeInstruction> InjectQuirkCapModModifiers(IEnumerable<CodeInstruction> instructions)
    //    {
    //        // literally just defer to the already existing patch that replaces field accesses to offset with the one that also polls the quirk manager
    //        foreach(CodeInstruction instruction in RimVore2.Patch_PawnCapacityUtility.InjectQuirkCapModModifiers(instructions))
    //            yield return instruction;
    //    }
    //}
}
